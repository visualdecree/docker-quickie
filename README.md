# Perch Simple Docker

This is a simple docker-compose setup for Perch and Perch Runway based on Rachel Andrews tutorial: https://perchrunway.com/blog/2017-01-19-getting-started-with-docker-for-local-development 

## Known issues

I found there to be a problem when using the dockerfile, whereby some dependencies related to GD were missing and causing images to not upload to Perch correctly (via the assets manager). I have modified the dockerfile with the updates and everything so far seems to work as I would expect. 

*2nd April 2017*
- Updated dockerfile with gd dependencies