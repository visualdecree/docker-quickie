'use strict';

(function (win, doc) {
  // Check if `fonts-loaded` cookie has been set
  if (doc.documentElement.className.indexOf('fonts-loaded') > -1) {
    return;
  }

  /*! Cookie function: get, set, or forget a cookie.
   * [c]2014 @scottjehl, Filament Group, Inc.
   * Licensed MIT
   */
  const cookie = function (name, value, days) {
    // If value is undefined, get the cookie value
    if (value === undefined) {
      const cookiestring = `; ${doc.cookie}`;
      const cookies = cookiestring.split(`; ${name}=`);
      if (cookies.length === 2) {
        return cookies.pop().split(';').shift();
      }
      return null;
    } else {
      // if value is a false boolean, we'll treat that as a delete
      if (value === false) {
        days = -1;
      }
      let expires = '';
      if (days) {
        const date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = `; expires=${date.toGMTString()}`;
      }
      doc.cookie = `${name}=${value}${expires}; path=/`;
    }
  };

  win.cookie = cookie;

  // Load webfonts
  // Requires FontFaceObserver (included during build)
  const merriweather400 = new FontFaceObserver('Merriweather', {
    weight: '400',
    style: 'normal',
  });

  const merriweather400i = new FontFaceObserver('Merriweather', {
    weight: '400',
    style: 'italic',
  });

  const roboto400 = new FontFaceObserver('Roboto', {
    weight: '400',
    style: 'normal',
  });

  const roboto400i = new FontFaceObserver('Roboto', {
    weight: '400',
    style: 'italic',
  });

  const roboto700 = new FontFaceObserver('Roboto', {
    weight: '700',
    style: 'normal',
  });

  const roboto700i = new FontFaceObserver('Roboto', {
    weight: '700',
    style: 'italic',
  });

  const urwdin = new FontFaceObserver('URWDIN-Demi', {
    weight: 'normal',
    style: 'normal',
  });

  Promise.all([
    merriweather400.load(),
    merriweather400i.load(),
    roboto400.load(),
    roboto400i.load(),
    roboto700.load(),
    roboto700i.load(),
    urwdin.load(),
  ]).then(function () {
    doc.documentElement.className += ' fonts-loaded';
    cookie('fonts-loaded', !0, 7);
  });
}(window, window.document));
